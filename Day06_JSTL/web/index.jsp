<%-- 
    Document   : index
    Created on : Feb 15, 2019, 2:45:56 PM
    Author     : LENOVO
--%>

<%@page import="DTO.RegistrationErrorObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Login Page</h1>
        <form action="MainController" method="POST">
            Username : <input type="text" name="txtUsername" value="" />
            <%
              RegistrationErrorObject errorObj = (RegistrationErrorObject)request.getAttribute("ERROR");
              if(errorObj!= null){
                 if(errorObj.getUsernameERROR()!=null){
              
            %>
            <font color="red"/>
            <%= errorObj.getUsernameERROR()%>
            </font>
            
            <%
                }
              }

            %>
            
            <br/>
            Password : <input type="password" name="txtPassword" value="" />
            <font color="red">
            ${requestScope.ERROR.passwordERROR}
            </font>
            <br/>
                  
            <input type="submit" name="action" value="Login" />
        </form>
            <a href="insert.jsp">Register account</a>
    </body>
</html>
