<%-- 
    Document   : update
    Created on : Feb 18, 2019, 3:38:47 PM
    Author     : Soai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="MainController" method="POST">
            Username: <input type="text" name="txtUsername" value="${requestScope.DTO.username}" readonly="true" />
            <br/>
            Fullname: <input type="text" name="txtFullname" value="${requestScope.DTO.fullname}" />
            <font color ="red">
            ${requestScope.INVALID.fullnameError}
            </font>
            <br/>
            Role: <input type="text" name="txtRole" value="${requestScope.DTO.role}" />
            <font color="red">
            ${requestScope.INVALID.roleError}
            </font>
            <br/>
            <input type="hidden" name="txtSearch" value="${param.txtSearch}" />
            <input type="submit" value="Update" name="action" />
        </form>
    </body>
</html>
