<%-- 
    Document   : test
    Created on : Feb 20, 2019, 3:27:06 PM
    Author     : Soai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <%@include file="body.jsp" %>
        <%@include file="right.jsp" %>
        <%@include file="footer.jsp" %>
    </body>
</html>
