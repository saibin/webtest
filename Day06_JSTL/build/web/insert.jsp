<%-- 
    Document   : insert
    Created on : Feb 20, 2019, 2:47:05 PM
    Author     : Soai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Hello World!</h2>
        <form action="MainController" method="POST">
            Username: <input type="text" name="txtUsername" value="${param.txtUsername}" />
            <font color="red">
            ${requestScope.INVALID.usernameERROR}
            </font>
            <br/>
            Password: <input type="password" name="txtPassword" value="" />
            <font color="red">
            ${requestScope.INVALID.passwordERROR}
            </font>
            <br/>
            Confirm Password: <input type="password" name="txtConfirm" value="" />
            <font color="red">
            ${requestScope.INVALID.confirmError}
            </font>
            <br/>
            
            Fullname: <input type="text" name="txtFullname" value="${param.txtFullname}" />
            <font color="red">
            ${requestScope.INVALID.fullnameError}
            </font>
            <br/>
            Role: <input type="text" name="txtRole" value="${param.txtRole}" />
            <font color="red">
            ${requestScope.INVALID.roleError}
            </font>
            <br/>
            <input type="submit" value="Insert" name="action" />
        </form>
    </body>
</html>
