/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import DAO.RegistrationDAO;
import DTO.RegistrationDTO;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class Beans implements Serializable {
    private String username, password;
    private String search;
    private RegistrationDTO dto;

    public RegistrationDTO getDto() {
        return dto;
    }

    public void setDto(RegistrationDTO dto) {
        this.dto = dto;
    }
    

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    
    public List<RegistrationDTO> findByLikeName() throws Exception{
        RegistrationDAO dao = new RegistrationDAO();
        return dao.findByLikeName(search);
    }
    
    public boolean delete() throws Exception{
        RegistrationDAO dao = new RegistrationDAO();
        return dao.delete(username);
    }
    
    public RegistrationDTO findByPrimaryKey() throws Exception{
        RegistrationDAO dao = new RegistrationDAO();
        return dao.findByPrimaryKey(username);
    }

    public boolean update() throws Exception{
        RegistrationDAO dao = new RegistrationDAO();
        return dao.update(dto);
    }
    
    public boolean insert() throws Exception{
        RegistrationDAO dao = new RegistrationDAO();
        return dao.insert(dto);
    }
    public Beans() {
    }
    
    public String checkLogin() throws Exception{
        RegistrationDAO DAO= new RegistrationDAO();
        return DAO.checkLogin(username, password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
