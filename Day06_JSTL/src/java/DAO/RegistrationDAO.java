/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Connection.MyConnection;
import DTO.RegistrationDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class RegistrationDAO implements Serializable{
    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    public RegistrationDAO() {
    }
    
    private void closeConnection() throws Exception{
        if(conn!=null){
            conn.close();
        }
        if(preStm!=null){
            preStm.close();
            
        }if(rs!=null){
            rs.close();
        }
    }
    public String checkLogin(String username, String password) throws Exception{
        String role="failed";
        try {
            String sql="Select Role From Registration Where Username = ? and Password = ?";
            conn= MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, username);
            preStm.setString(2, password);
            rs=preStm.executeQuery();
            if(rs.next())
                role=rs.getString("Role");
                    
        } finally{
            closeConnection();
        
    }
        return role;
    
            
    } 
    
    public List<RegistrationDTO> findByLikeName(String search) throws Exception{
        List<RegistrationDTO> result =null;
        String username = null;
        String role = null;
        String fullname = null;
        RegistrationDTO dto = null;
        try {
            String sql = "Select Username , Fullname, Role From Registration " + "Where Fullname like ?";
            conn = MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, "%" + search + "%");
            rs = preStm.executeQuery();
            result = new ArrayList<>();
            while(rs.next()){
                username = rs.getString("Username");
                fullname = rs.getString("Fullname");
                role = rs.getString("Role");
                dto = new RegistrationDTO(username, fullname, role);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public boolean delete(String id) throws Exception{
        boolean check = false;
        try {
            String sql = "Delete From Registration Where Username = ?";
            conn = MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            check = preStm.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
    
    public RegistrationDTO findByPrimaryKey (String username) throws Exception{
        RegistrationDTO dto = null;
        try {
            String sql = "Select Fullname, Role From Registration "
                    + "Where Username = ?";
            conn = MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, username);
            rs=preStm.executeQuery();
            if(rs.next()){
                String fullname = rs.getString("Fullname");
                String role = rs.getString("Role");
                dto = new RegistrationDTO(username, fullname, role);
            }
        } finally{
            closeConnection();
        }
        return dto;
    }
    public boolean update(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try {
            String sql = "Update Registration set Fullname = ?, Role = ? Where Username = ? ";
            conn = MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, dto.getFullname());
            preStm.setString(2, dto.getRole());
            preStm.setString(3, dto.getUsername());
            check = preStm.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
    public boolean insert(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try {
            String sql ="Insert Into Registration(Username, Password, Fullname, Role)"
                    + " values(?,?,?,?)";
            conn = MyConnection.getMyConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, dto.getUsername());
            preStm.setString(2, dto.getPassword());
            preStm.setString(3, dto.getFullname());
            preStm.setString(4, dto.getRole());
            check = preStm.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
}
