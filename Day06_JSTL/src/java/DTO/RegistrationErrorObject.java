/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;

/**
 *
 * @author LENOVO
 */
public class RegistrationErrorObject implements Serializable {
    private String usernameERROR, passwordERROR;
    private String fullnameError, roleError, confirmError;

    public String getFullnameError() {
        return fullnameError;
    }

    public String getConfirmError() {
        return confirmError;
    }

    public void setConfirmError(String confirmError) {
        this.confirmError = confirmError;
    }

    public void setFullnameError(String fullnameError) {
        this.fullnameError = fullnameError;
    }

    public String getRoleError() {
        return roleError;
    }

    public void setRoleError(String roleError) {
        this.roleError = roleError;
    }

    
    public RegistrationErrorObject() {
    }

    public RegistrationErrorObject(String usernameERROR, String passwordERROR) {
        this.usernameERROR = usernameERROR;
        this.passwordERROR = passwordERROR;
    }

    public String getUsernameERROR() {
        return usernameERROR;
    }

    public void setUsernameERROR(String usernameERROR) {
        this.usernameERROR = usernameERROR;
    }

    public String getPasswordERROR() {
        return passwordERROR;
    }

    public void setPasswordERROR(String passwordERROR) {
        this.passwordERROR = passwordERROR;
    }
    
}
